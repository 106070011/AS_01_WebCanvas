var canvas = document.getElementById('art');
var ctx = canvas.getContext('2d');
var tempCanvas = document.getElementById("img_temp");
var tempContext = tempCanvas.getContext("2d");
var status = "pen";

function switchStatus(val){
    status = val;
    console.log(status);
}

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);
  ctx.lineTo(mousePos.x, mousePos.y);
  ctx.stroke();
}

var startX, endX, startY, endY;
var mouseIsDown = 0;

tempCanvas.addEventListener("mousedown", function(evt) {
    if(status == "pen" || status == "eraser" || status == "highlight"){
        var mousePos = getMousePos(canvas, evt);
        ctx.beginPath();
        ctx.moveTo(mousePos.x, mousePos.y);
        evt.preventDefault();
        tempCanvas.addEventListener('mousemove', mouseMove, false);
    }
    else if(status == "circle" || status == "rectangle" || status == "triangle" || status == "line"){
        mouseDown(evt);
        tempCanvas.addEventListener("mousemove", mouseXY, false);
    }
}, false);

tempCanvas.addEventListener('mouseup', function(eve) {
    if(status == "pen" || status == "eraser" || status == "highlight"){
      tempCanvas.removeEventListener('mousemove', mouseMove, false);
      cPush();
    }
    else if(status == "circle" || status == "rectangle" || status == "triangle" || status == "line"){
        mouseUp(eve);
        tempCanvas.removeEventListener("mousemove", mouseXY, false);
        cPush();
    }
}, false);


function mouseUp(eve) {
    if (mouseIsDown !== 0) {
        mouseIsDown = 0;
        var pos = getMousePos(canvas, eve);
        endX = pos.x;
        endY = pos.y;
        draw(true); //update on mouse-up
    }
}

function mouseDown(eve) {
    mouseIsDown = 1;
    var pos = getMousePos(canvas, eve);
    startX = endX = pos.x;
    startY = endY = pos.y;
    draw(false); //update

}

function mouseXY(eve) {

    if (mouseIsDown !== 0) {
        var pos = getMousePos(canvas, eve);
        endX = pos.x;
        endY = pos.y;
        draw(false);
    }
}

var shape = "stroke";

function changeShape(val){
    shape = val;
};

function draw(final) {
    var context = final ? ctx : tempContext;
    // final draw
    if (final == true) {
        // clear temp canvas
        tempContext.clearRect(0, 0, canvas.width, canvas.height);
    }
    // temporary draw
    if (final == false) {
        context.clearRect(0, 0, canvas.width, canvas.height);
    }

    if(status == "circle"){
        radius = getDistance(startX, startY, endX, endY);
        context.beginPath();
        context.arc(startX, startY, radius, 0, 2 * Math.PI);
        context.closePath();
        if(shape == "stroke") context.stroke();
        else context.fill();
    }
    else if(status == "rectangle"){
        if(shape == "stroke") context.strokeRect(startX, startY, endX-startX, endY-startY);
        else context.fillRect(startX, startY, endX-startX, endY-startY);
    }
    else if(status == "triangle"){
        context.beginPath();
        context.moveTo(startX,startY);
        context.lineTo(endX, endY);
        context.lineTo(-endX+2*startX, endY);
        context.closePath();
        if(shape == "stroke") context.stroke();
        else context.fill();
    }
    else if(status == "line"){
        context.beginPath();
        context.moveTo(startX,startY);
        context.lineTo(endX, endY);
        context.closePath();
        context.stroke();
    }
}

function getDistance(p1X, p1Y, p2X, p2Y) {
    return Math.sqrt(Math.pow(p1X - p2X, 2) + Math.pow(p1Y - p2Y, 2))
}

document.getElementById('reset').addEventListener('click', function() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  cPush.length = 0;
  cStep = -1;
}, false);

var r =0;
var g =0;
var b =0;

function getR(val){
    r = val;
    changeColor();
};
function getG(val){
    g = val;
    changeColor();
};
function getB(val){
    b = val;
    changeColor();
};

function changeColor(){
    if(status == "eraser"){
        ctx.strokeStyle = "rgb(255,255,255)";
        ctx.fillStyle = "rgb(255,255,255)";
        tempContext.strokeStyle = "rgb(255,255,255)";
        tempContext.fillStyle = "rgb(255,255,255)";
    }
    else if(status == "highlight"){
        ctx.strokeStyle = "rgba("+r+","+g+","+b+",0.05)";
        ctx.fillStyle = "rgba("+r+","+g+","+b+",0.05)";
        tempContext.strokeStyle = "rgba("+r+","+g+","+b+",0.05)";
        tempContext.fillStyle = "rgba("+r+","+g+","+b+",0.05)";
    }
    else{
        ctx.strokeStyle = "rgb("+r+","+g+","+b+")";
        ctx.fillStyle = "rgb("+r+","+g+","+b+")";
        tempContext.strokeStyle = "rgb("+r+","+g+","+b+")";
        tempContext.fillStyle = "rgb("+r+","+g+","+b+")";
    }
    console.log(r,g,b);
};

//brush_size
function brushSize(val) {
    val = Number(val);
    ctx.lineWidth = val;
    tempContext.lineWidth = val;
    console.log(typeof(val));
};

//Upload
var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);

function handleImage(e){ //undone: move around
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            tempCanvas.onclick = function (evt) {
                var mousePos = getMousePos(canvas, evt);
                ctx.drawImage(img,mousePos.x,mousePos.y);
                cPush();
                this.onclick = null;
            };
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);   
};

//Download
function download() {
    var download = document.getElementById("download");
    var image = canvas.toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
    //download.setAttribute("download","archive.png");
};

//Change cursor
function changeCursor(url){
    canvas.style.cursor = "url("+url+"),auto";
    tempCanvas.style.cursor = "url("+url+"),auto";
};

//Undo/redo
var cPushArray = new Array();
var cStep = -1;
	
function cPush() {
    console.log("push!");
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(canvas.toDataURL());
};

function cUndo() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
    else if(cStep == 0){
        cStep--;
    }
};

function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
};

//text
var hasInput = false;
var font_family = "Arial";
var font_size = "12px";

function fontFam(val){
    font_family = val;
}

function fontSize(val){
    font_size = val;
}

tempCanvas.onclick = function(e) {
    var rect = canvas.getBoundingClientRect();
    if (hasInput || status !=="txt") return;
    addInput(e.clientX - rect.left, e.clientY - rect.top); 
};

function addInput(x, y) {

    var input = document.createElement('input');

    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x - 4) + 'px';
    input.style.top = (y - 4) + 'px';

    input.onkeydown = handleEnter;

    document.body.appendChild(input);

    input.focus();

    hasInput = true;
};

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        hasInput = false;
    }
};

function drawText(txt, x, y) {
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = font_size + " " + font_family;
    ctx.fillText(txt, x - 4, y - 4);
    cPush();
};


let hue = 0;

function rainbow(){
    ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    hue++;
    if (hue >= 360) {
        hue = 0;
    }
}