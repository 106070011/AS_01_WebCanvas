# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
## Functions
### 1. Basic Tools (30%): Done ###
- **Brush and eraser**
Click the pencil/eraser/highlighter icon to use the brush/eraser.
![](https://i.imgur.com/66o5fik.png)

:smile: **MORE:** Additional brush: Highlighter

- **Color selector**
Select brush color by using R,G,B three input range bars.
![](https://i.imgur.com/eDIXpyH.png)
- **Simple menu (brush size)**
Customize brush size using the input range bar.
![](https://i.imgur.com/7HxrgoI.png)
### 2. Text input (10%): Done ###
**Usage**
- First, click the "T" icon to invoke text function**
- Then, click on anywhere on the canvas, and type your text on the appeared typing space
- Press Enter to show on the canvas

**Customization**
- Use the selector to select *font-family* and *font-size*
![](https://i.imgur.com/IKWzrE2.png)

:smile: **MORE:** There is a dynamic **typing space!** and **Enter!**

### 3. Cursor icon (10%): Done ###
![](https://i.imgur.com/iYzrxim.png): pencil, highlight, line function
![](https://i.imgur.com/7HS4WNa.png): eraser function
![](https://i.imgur.com/7rQ9aFX.png):different shape according to circle, rectangle, triangle function
### 4. Refresh button (10%): Done ###
![](https://i.imgur.com/mufWMuc.png) Refresh the whole canvas by clicking the icon
### 5. Different brush shapes (15%): Done ###
Choose either a filled shape or a lined shape by radio inputs 
- **Circle**
- **Rectangle**
- **Triangle**
- **Line**
![](https://i.imgur.com/coaZkbH.png)

:smile: **MORE:** 
1) Additional shape: Straight Line
2) Two Kinds of Shape: Fill/ Stroke 
### 6. Un/Re-do button (10%): Done ###
![](https://i.imgur.com/rsBTJHS.png) Click Undo/Redo icons(It will not undo after the first step or after reset.)
### 7. Image tool (5%) & Download (5%): Done ###
- **Upload**
Click upload icon, chose file, and click where you want to put on the canvas to paste it
- **Download**
Click download icon, the canvas will download as a **.jpg** file
![](https://i.imgur.com/XqHpBFe.png)
:smile: **MORE:** The canvas is **shadowed!** 
